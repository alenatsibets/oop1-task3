import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Enter three variables");
        Scanner scanner = new Scanner(System.in);
        int val1 = scanner.nextInt();
        int val2 = scanner.nextInt();
        int val3 = scanner.nextInt();
        Triangle a = new Triangle(val1, val2, val3);
        int perimeter = a.calculatingThePerimeter();
        System.out.println("The perimeter is: " + perimeter);
        int area = a.calculatingTheArea();
        System.out.println("The area is: " + area);
    }
}