public class Triangle {
    private final int side1;
    private final int side2;
    private final int side3;

    public Triangle(int side1, int side2, int side3) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    public int getSide1() {
        return side1;
    }

    public int getSide2() {
        return side2;
    }

    public int getSide3() {
        return side3;
    }

    public int calculatingThePerimeter(){
        return this.getSide1() + this.getSide2() + this.getSide3();
    }
    public int calculatingTheArea(){
        int p = this.calculatingThePerimeter()/ 2;
        return  (int) Math.sqrt(p * (p - this.getSide1()) * (p - this.getSide2()) * (p - this.getSide3()));
    }
}
